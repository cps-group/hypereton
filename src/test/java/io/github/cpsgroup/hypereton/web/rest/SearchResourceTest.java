//package io.github.cpsgroup.hypereton.web.rest;
//
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//import javax.inject.Inject;
//
//import org.joda.time.LocalDate;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.MockitoAnnotations;
//import org.springframework.boot.test.SpringApplicationConfiguration;
//import org.springframework.http.MediaType;
//import org.springframework.test.context.ActiveProfiles;
//import org.springframework.test.context.TestExecutionListeners;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
//import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
//import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
//import org.springframework.test.context.web.WebAppConfiguration;
//import org.springframework.test.util.ReflectionTestUtils;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//
//import io.github.cpsgroup.hypereton.Application;
//import io.github.cpsgroup.hypereton.domain.Search;
//import io.github.cpsgroup.hypereton.repository.SearchRepository;
//
//
///**
// * Test class for the SearchResource REST controller.
// *
// * @see SearchResource
// */
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringApplicationConfiguration(classes = Application.class)
//@WebAppConfiguration
//@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
//    DirtiesContextTestExecutionListener.class,
//    TransactionalTestExecutionListener.class })
//@ActiveProfiles("dev")
//public class SearchResourceTest {
//    
//    private static final Long DEFAULT_ID = new Long(1L);
//
//    private static final LocalDate DEFAULT_SAMPLE_DATE_ATTR = new LocalDate(0L);
//
//    private static final LocalDate UPD_SAMPLE_DATE_ATTR = new LocalDate();
//
//    private static final String DEFAULT_SAMPLE_TEXT_ATTR = "sampleTextAttribute";
//
//    private static final String UPD_SAMPLE_TEXT_ATTR = "sampleTextAttributeUpt";
//
//    @Inject
//    private SearchRepository searchRepository;
//
//    private MockMvc restSearchMockMvc;
//    
//    private Search search;
//
//    @Before
//    public void setup() {
//        MockitoAnnotations.initMocks(this);
//        SearchResource searchResource = new SearchResource();
//        ReflectionTestUtils.setField(searchResource, "searchRepository", searchRepository);
//
//        this.restSearchMockMvc = MockMvcBuilders.standaloneSetup(searchResource).build();
//
//        search = new Search();
//        search.setId(DEFAULT_ID);
//    	search.setSampleDateAttribute(DEFAULT_SAMPLE_DATE_ATTR);
//    	search.setSampleTextAttribute(DEFAULT_SAMPLE_TEXT_ATTR);
//    }
//
//    @Test
//    public void testCRUDSearch() throws Exception {
//
//    	// Create Search
//    	restSearchMockMvc.perform(post("/app/rest/searchs")
//    			.contentType(TestUtil.APPLICATION_JSON_UTF8)
//                .content(TestUtil.convertObjectToJsonBytes(search)))
//                .andExpect(status().isOk());
//
//    	// Read Search
//    	restSearchMockMvc.perform(get("/app/rest/searchs/{id}", DEFAULT_ID))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$.id").value(DEFAULT_ID.intValue()))
//    			.andExpect(jsonPath("$.sampleDateAttribute").value(DEFAULT_SAMPLE_DATE_ATTR.toString()))
//    			.andExpect(jsonPath("$.sampleTextAttribute").value(DEFAULT_SAMPLE_TEXT_ATTR));
//
//    	// Update Search
//    	search.setSampleDateAttribute(UPD_SAMPLE_DATE_ATTR);
//    	search.setSampleTextAttribute(UPD_SAMPLE_TEXT_ATTR);
//  
//    	restSearchMockMvc.perform(post("/app/rest/searchs")
//    			.contentType(TestUtil.APPLICATION_JSON_UTF8)
//                .content(TestUtil.convertObjectToJsonBytes(search)))
//                .andExpect(status().isOk());
//
//    	// Read updated Search
//    	restSearchMockMvc.perform(get("/app/rest/searchs/{id}", DEFAULT_ID))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$.id").value(DEFAULT_ID.intValue()))
//    			.andExpect(jsonPath("$.sampleDateAttribute").value(UPD_SAMPLE_DATE_ATTR.toString()))
//    			.andExpect(jsonPath("$.sampleTextAttribute").value(UPD_SAMPLE_TEXT_ATTR));
//
//    	// Delete Search
//    	restSearchMockMvc.perform(delete("/app/rest/searchs/{id}", DEFAULT_ID)
//                .accept(TestUtil.APPLICATION_JSON_UTF8))
//                .andExpect(status().isOk());
//
//    	// Read nonexisting Search
//    	restSearchMockMvc.perform(get("/app/rest/searchs/{id}", DEFAULT_ID)
//                .accept(TestUtil.APPLICATION_JSON_UTF8))
//                .andExpect(status().isNotFound());
//
//    }
//}
