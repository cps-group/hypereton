//package io.github.cpsgroup.hypereton.web.rest;
//
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//import javax.inject.Inject;
//
//import org.joda.time.LocalDate;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.MockitoAnnotations;
//import org.springframework.boot.test.SpringApplicationConfiguration;
//import org.springframework.http.MediaType;
//import org.springframework.test.context.ActiveProfiles;
//import org.springframework.test.context.TestExecutionListeners;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
//import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
//import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
//import org.springframework.test.context.web.WebAppConfiguration;
//import org.springframework.test.util.ReflectionTestUtils;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//
//import io.github.cpsgroup.hypereton.Application;
//import io.github.cpsgroup.hypereton.domain.Law;
//import io.github.cpsgroup.hypereton.repository.LawRepository;
//
//
///**
// * Test class for the LawResource REST controller.
// *
// * @see LawResource
// */
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringApplicationConfiguration(classes = Application.class)
//@WebAppConfiguration
//@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
//    DirtiesContextTestExecutionListener.class,
//    TransactionalTestExecutionListener.class })
//@ActiveProfiles("dev")
//public class LawResourceTest {
//    
//    private static final Long DEFAULT_ID = new Long(1L);
//
//    private static final LocalDate DEFAULT_SAMPLE_DATE_ATTR = new LocalDate(0L);
//
//    private static final LocalDate UPD_SAMPLE_DATE_ATTR = new LocalDate();
//
//    private static final String DEFAULT_SAMPLE_TEXT_ATTR = "sampleTextAttribute";
//
//    private static final String UPD_SAMPLE_TEXT_ATTR = "sampleTextAttributeUpt";
//
//    @Inject
//    private LawRepository lawRepository;
//
//    private MockMvc restLawMockMvc;
//    
//    private Law law;
//
//    @Before
//    public void setup() {
//        MockitoAnnotations.initMocks(this);
//        LawResource lawResource = new LawResource();
//        ReflectionTestUtils.setField(lawResource, "lawRepository", lawRepository);
//
//        this.restLawMockMvc = MockMvcBuilders.standaloneSetup(lawResource).build();
//
//        law = new Law();
//        law.setId(DEFAULT_ID);
//    	law.setRatificationDate(DEFAULT_SAMPLE_DATE_ATTR);
//    	law.setTitle(DEFAULT_SAMPLE_TEXT_ATTR);
//    }
//
//    @Test
//    public void testCRUDLaw() throws Exception {
//
//    	// Create Law
//    	restLawMockMvc.perform(post("/app/rest/laws")
//    			.contentType(TestUtil.APPLICATION_JSON_UTF8)
//                .content(TestUtil.convertObjectToJsonBytes(law)))
//                .andExpect(status().isOk());
//
//    	// Read Law
//    	restLawMockMvc.perform(get("/app/rest/laws/{id}", DEFAULT_ID))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$.id").value(DEFAULT_ID.intValue()))
//    			.andExpect(jsonPath("$.sampleDateAttribute").value(DEFAULT_SAMPLE_DATE_ATTR.toString()))
//    			.andExpect(jsonPath("$.sampleTextAttribute").value(DEFAULT_SAMPLE_TEXT_ATTR));
//
//    	// Update Law
//    	law.setRatificationDate(UPD_SAMPLE_DATE_ATTR);
//    	law.setTitle(UPD_SAMPLE_TEXT_ATTR);
//  
//    	restLawMockMvc.perform(post("/app/rest/laws")
//    			.contentType(TestUtil.APPLICATION_JSON_UTF8)
//                .content(TestUtil.convertObjectToJsonBytes(law)))
//                .andExpect(status().isOk());
//
//    	// Read updated Law
//    	restLawMockMvc.perform(get("/app/rest/laws/{id}", DEFAULT_ID))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$.id").value(DEFAULT_ID.intValue()))
//    			.andExpect(jsonPath("$.sampleDateAttribute").value(UPD_SAMPLE_DATE_ATTR.toString()))
//    			.andExpect(jsonPath("$.sampleTextAttribute").value(UPD_SAMPLE_TEXT_ATTR));
//
//    	// Delete Law
//    	restLawMockMvc.perform(delete("/app/rest/laws/{id}", DEFAULT_ID)
//                .accept(TestUtil.APPLICATION_JSON_UTF8))
//                .andExpect(status().isOk());
//
//    	// Read nonexisting Law
//    	restLawMockMvc.perform(get("/app/rest/laws/{id}", DEFAULT_ID)
//                .accept(TestUtil.APPLICATION_JSON_UTF8))
//                .andExpect(status().isNotFound());
//
//    }
//}
