//package io.github.cpsgroup.hypereton.web.rest;
//
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//import javax.inject.Inject;
//
//import org.joda.time.LocalDate;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.MockitoAnnotations;
//import org.springframework.boot.test.SpringApplicationConfiguration;
//import org.springframework.http.MediaType;
//import org.springframework.test.context.ActiveProfiles;
//import org.springframework.test.context.TestExecutionListeners;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
//import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
//import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
//import org.springframework.test.context.web.WebAppConfiguration;
//import org.springframework.test.util.ReflectionTestUtils;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//
//import io.github.cpsgroup.hypereton.Application;
//import io.github.cpsgroup.hypereton.domain.View;
//import io.github.cpsgroup.hypereton.repository.ViewRepository;
//
//
///**
// * Test class for the ViewResource REST controller.
// *
// * @see ViewResource
// */
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringApplicationConfiguration(classes = Application.class)
//@WebAppConfiguration
//@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
//    DirtiesContextTestExecutionListener.class,
//    TransactionalTestExecutionListener.class })
//@ActiveProfiles("dev")
//public class ViewResourceTest {
//    
//    private static final Long DEFAULT_ID = new Long(1L);
//
//    private static final LocalDate DEFAULT_SAMPLE_DATE_ATTR = new LocalDate(0L);
//
//    private static final LocalDate UPD_SAMPLE_DATE_ATTR = new LocalDate();
//
//    private static final String DEFAULT_SAMPLE_TEXT_ATTR = "sampleTextAttribute";
//
//    private static final String UPD_SAMPLE_TEXT_ATTR = "sampleTextAttributeUpt";
//
//    @Inject
//    private ViewRepository viewRepository;
//
//    private MockMvc restViewMockMvc;
//    
//    private View view;
//
//    @Before
//    public void setup() {
//        MockitoAnnotations.initMocks(this);
//        ViewResource viewResource = new ViewResource();
//        ReflectionTestUtils.setField(viewResource, "viewRepository", viewRepository);
//
//        this.restViewMockMvc = MockMvcBuilders.standaloneSetup(viewResource).build();
//
//        view = new View();
//        view.setId(DEFAULT_ID);
//    	view.setEditDate(DEFAULT_SAMPLE_DATE_ATTR);
//    	view.setTitle(DEFAULT_SAMPLE_TEXT_ATTR);
//    }
//
//    @Test
//    public void testCRUDView() throws Exception {
//
//    	// Create View
//    	restViewMockMvc.perform(post("/app/rest/views")
//    			.contentType(TestUtil.APPLICATION_JSON_UTF8)
//                .content(TestUtil.convertObjectToJsonBytes(view)))
//                .andExpect(status().isOk());
//
//    	// Read View
//    	restViewMockMvc.perform(get("/app/rest/views/{id}", DEFAULT_ID))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$.id").value(DEFAULT_ID.intValue()))
//    			.andExpect(jsonPath("$.sampleDateAttribute").value(DEFAULT_SAMPLE_DATE_ATTR.toString()))
//    			.andExpect(jsonPath("$.sampleTextAttribute").value(DEFAULT_SAMPLE_TEXT_ATTR));
//
//    	// Update View
//    	view.setEditDate(UPD_SAMPLE_DATE_ATTR);
//    	view.setTitle(UPD_SAMPLE_TEXT_ATTR);
//  
//    	restViewMockMvc.perform(post("/app/rest/views")
//    			.contentType(TestUtil.APPLICATION_JSON_UTF8)
//                .content(TestUtil.convertObjectToJsonBytes(view)))
//                .andExpect(status().isOk());
//
//    	// Read updated View
//    	restViewMockMvc.perform(get("/app/rest/views/{id}", DEFAULT_ID))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$.id").value(DEFAULT_ID.intValue()))
//    			.andExpect(jsonPath("$.sampleDateAttribute").value(UPD_SAMPLE_DATE_ATTR.toString()))
//    			.andExpect(jsonPath("$.sampleTextAttribute").value(UPD_SAMPLE_TEXT_ATTR));
//
//    	// Delete View
//    	restViewMockMvc.perform(delete("/app/rest/views/{id}", DEFAULT_ID)
//                .accept(TestUtil.APPLICATION_JSON_UTF8))
//                .andExpect(status().isOk());
//
//    	// Read nonexisting View
//    	restViewMockMvc.perform(get("/app/rest/views/{id}", DEFAULT_ID)
//                .accept(TestUtil.APPLICATION_JSON_UTF8))
//                .andExpect(status().isNotFound());
//
//    }
//}
