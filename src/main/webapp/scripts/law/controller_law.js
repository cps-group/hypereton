'use strict';

var allTheLaws;

hyperetonApp.controller('LawController', ['$scope', 'resolvedLaw', 'Law',
    function ($scope, resolvedLaw, Law) {

        $scope.laws = resolvedLaw;
        allTheLaws = resolvedLaw;

        $scope.create = function () {
            Law.save($scope.law,
                function () {
                    $scope.laws = Law.query();
                    $('#saveLawModal').modal('hide');
                    $scope.clear();
                });
        };

        $scope.update = function (id) {
            $scope.law = Law.get({id: id});
            $('#saveLawModal').modal('show');
        };

        $scope.delete = function (id) {
            Law.delete({id: id},
                function () {
                    $scope.laws = Law.query();
                });
        };

        $scope.clear = function () {
            $scope.law = {id: null, title: "Law", ratificationDate: "2002-02-02", customFlag: true, numSections: -1};
        };
    }]);

function myFunction() {
	document.getElementById("chart").innerHTML = Date();
}

function showChart() {
	
//	var scope = angular.element($('[ng-controller=LawController]')).scope();
//	scope.$apply(function() {
//		   scope.info = "info";
//		  scope.updateMessage("hi there");
//		  });
//	
//	var currentLaws = scope.laws();
//	console.log(scope);
	
	var currentLaws = allTheLaws;
	
	// Create the data table.
	var finalArray = new Array(70);
	for (var i = 0; i < 70; i++) {
		finalArray[i] = new Array(2);
	}
	finalArray[0][0] = "Year";
	finalArray[0][1] = "Sections";
	
	//initialize array with years in the first column and all zeros as counted sections
	for (var i = 1; i < 70; i++) {
		finalArray[i][0] = (1945 + i);
		finalArray[i][1] = 0;
	}
	
	for (var i = 0; i < currentLaws.length; i++) {
		var currentDate = currentLaws[i].ratificationDate;
		var currentSections = currentLaws[i].numSections;
		var year = 0;
		year += currentDate[0] * 1000;
		year += currentDate[1] * 100;
		year += currentDate[2] * 10;
		year += currentDate[3] * 1;
		
		finalArray[year - 1945][1] += currentSections;
	}
			
	var data = new google.visualization.arrayToDataTable(finalArray);
	var options = {
			width: '1000px',
			hAxis: {title: data.getColumnLabel(0)}
	};

   // Instantiate and draw our chart, passing in some options.
   var chart = new google.visualization.ColumnChart(document.getElementById('chart'));
   chart.draw(data, options);
}
