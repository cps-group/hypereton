'use strict';

hyperetonApp.factory('Law', ['$resource',
    function ($resource) {
        return $resource('app/rest/laws/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': { method: 'GET'}
        });
    }]);
