'use strict';

hyperetonApp
    .config(['$routeProvider', '$httpProvider', '$translateProvider', 'USER_ROLES',
        function ($routeProvider, $httpProvider, $translateProvider, USER_ROLES) {
            $routeProvider
                .when('/law', {
                    templateUrl: 'views/laws.html',
                    controller: 'LawController',
                    resolve:{
                        resolvedLaw: ['Law', function (Law) {
                            return Law.query();
                        }]
                    },
                    access: {
                        authorizedRoles: [USER_ROLES.all]
                    }
                })
        }]);
