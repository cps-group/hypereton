'use strict';

hyperetonApp
    .config(['$routeProvider', '$httpProvider', '$translateProvider', 'USER_ROLES',
        function ($routeProvider, $httpProvider, $translateProvider, USER_ROLES) {
            $routeProvider
                .when('/cookie', {
                    templateUrl: 'views/cookies.html',
                    controller: 'CookieController',
                    resolve:{
                        resolvedCookie: ['Cookie', function (Cookie) {
                            return Cookie.query();
                        }]
                    },
                    access: {
                        authorizedRoles: [USER_ROLES.all]
                    }
                })
        }]);
