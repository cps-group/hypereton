'use strict';

hyperetonApp.factory('Cookie', ['$resource',
    function ($resource) {
        return $resource('app/rest/cookies/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': { method: 'GET'}
        });
    }]);
