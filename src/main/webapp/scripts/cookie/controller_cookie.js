'use strict';

hyperetonApp.controller('CookieController', ['$scope', 'resolvedCookie', 'Cookie',
    function ($scope, resolvedCookie, Cookie) {

        $scope.cookies = resolvedCookie;

        $scope.create = function () {
            Cookie.save($scope.cookie,
                function () {
                    $scope.cookies = Cookie.query();
                    $('#saveCookieModal').modal('hide');
                    $scope.clear();
                });
        };

        $scope.update = function (id) {
            $scope.cookie = Cookie.get({id: id});
            $('#saveCookieModal').modal('show');
        };

        $scope.delete = function (id) {
            Cookie.delete({id: id},
                function () {
                    $scope.cookies = Cookie.query();
                });
        };

        $scope.clear = function () {
            $scope.cookie = {id: null, sampleTextAttribute: null, sampleDateAttribute: null};
        };
    }]);
