'use strict';

hyperetonApp.factory('Search', ['$resource',
    function ($resource) {
        return $resource('app/rest/searchs/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': { method: 'GET'}
        });
    }]);
