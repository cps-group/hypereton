'use strict';

hyperetonApp
    .config(['$routeProvider', '$httpProvider', '$translateProvider', 'USER_ROLES',
        function ($routeProvider, $httpProvider, $translateProvider, USER_ROLES) {
            $routeProvider
                .when('/search', {
                    templateUrl: 'views/searchs.html',
                    controller: 'SearchController',
                    resolve:{
                        resolvedSearch: ['Search', function (Search) {
                            return Search.query();
                        }]
                    },
                    access: {
                        authorizedRoles: [USER_ROLES.all]
                    }
                })
        }]);
