'use strict';

hyperetonApp.controller('SearchController', ['$scope', 'resolvedSearch', 'Search',
    function ($scope, resolvedSearch, Search) {

        $scope.searchs = resolvedSearch;

        $scope.create = function () {
            Search.save($scope.search,
                function () {
                    $scope.searchs = Search.query();
                    $('#saveSearchModal').modal('hide');
                    $scope.clear();
                });
        };

        $scope.startpause = function (id) {
        	Search.get({id: id},
        			function () {
        				$scope.searchs = Search.query();
        		});
        };

        $scope.delete = function (id) {
            Search.delete({id: id},
                function () {
                    $scope.searchs = Search.query();
                });
        };

        $scope.clear = function () {
            $scope.search = {id: null, searchTerm: "Lehrerbildung AND Bologna", dataProvider: "Makrolog"};
        };
    }]);
