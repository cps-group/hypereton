'use strict';

hyperetonApp.factory('View', ['$resource',
    function ($resource) {
        return $resource('app/rest/views/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': { method: 'GET'}
        });
    }]);
