'use strict';

hyperetonApp
    .config(['$routeProvider', '$httpProvider', '$translateProvider', 'USER_ROLES',
        function ($routeProvider, $httpProvider, $translateProvider, USER_ROLES) {
            $routeProvider
                .when('/view', {
                    templateUrl: 'views/views.html',
                    controller: 'ViewController',
                    resolve:{
                        resolvedView: ['View', function (View) {
                            return View.query();
                        }]
                    },
                    access: {
                        authorizedRoles: [USER_ROLES.all]
                    }
                })
        }]);
