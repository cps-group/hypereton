'use strict';

hyperetonApp.controller('ViewController', ['$scope', 'resolvedView', 'View',
    function ($scope, resolvedView, View) {

        $scope.views = resolvedView;

        $scope.create = function () {
            View.save($scope.view,
                function () {
                    $scope.views = View.query();
                    $('#saveViewModal').modal('hide');
                    $scope.clear();
                });
        };

        $scope.update = function (id) {
            $scope.view = View.get({id: id});
            $('#saveViewModal').modal('show');
        };

        $scope.delete = function (id) {
            View.delete({id: id},
                function () {
                    $scope.views = View.query();
                });
        };

        $scope.clear = function () {
            $scope.view = {id: null, title: "title", description: "description"};
        };
    }]);
