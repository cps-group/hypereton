package io.github.cpsgroup.hypereton.service;

import java.util.List;

import io.github.cpsgroup.hypereton.domain.Cookie;
import io.github.cpsgroup.hypereton.domain.Law;
import io.github.cpsgroup.hypereton.domain.Search;
import io.github.cpsgroup.hypereton.domain.User;
import io.github.cpsgroup.hypereton.domain.View;
import io.github.cpsgroup.hypereton.repository.CookieRepository;
import io.github.cpsgroup.hypereton.repository.LawRepository;
import io.github.cpsgroup.hypereton.repository.SearchRepository;
import io.github.cpsgroup.hypereton.repository.UserRepository;
import io.github.cpsgroup.hypereton.repository.ViewRepository;
import io.github.cpsgroup.hypereton.security.SecurityUtils;

import javax.inject.Inject;

import org.hibernate.Hibernate;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CookieService {
    private final Logger log = LoggerFactory.getLogger(CookieService.class);    
    @Inject
    private UserRepository userRepo;    
    @Inject
    private ViewRepository viewRepo;    
    @Inject
    private CookieRepository cookieRepo;    
    @Inject
    private LawRepository lawRepo;
    @Inject
    private SearchRepository searchRepo;
    
    
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    /**
     * Get's the Cookie of th currently logged in user 
     * Attention: A cookie needs a currentView, otherwise it
     * can't show the content of the current view!
     * @return Cookie
     */
    public Cookie getUserCookie() {
    	User currentUser = userRepo.findOne(SecurityUtils.getCurrentLogin());
    	return cookieRepo.findCookieByUser(currentUser);
    }    
    
    /**
     * Creates a new cookie if the user has no cookie yet
     */
    public Cookie setUserCookie() { 
    	User currentUser = userRepo.findOne(SecurityUtils.getCurrentLogin());
    	Cookie cookie = cookieRepo.findCookieByUser(currentUser);
    	if(cookie == null){
    		cookie = new Cookie();
    		cookie.setUser(currentUser);
    		cookieRepo.save(cookie);
    		}
    	return cookie;
    }    
        
    /**
     * Retrieves the currentView of userCookie
     * @return the View
     */
    public View getCurrentView() {
    	Long id = getUserCookie().getCurrentViewId();
    	if (id == null)
    		return null;
    	return viewRepo.getOne(id);
    } 
    
    /**
     * Always change/set the currentView when the user changes views
     * @param currentView
     */
    public void setCurrentView(View currentView) { 
    	Cookie cookie = getUserCookie();
    	cookie.setCurrentViewId(currentView.getId());
    	cookieRepo.save(cookie);
    }
    
    /**
     * Always add every view the user has created to the "ViewList"
     * It connects every view with the usercookie Id
     * @param view
     */
    public void addViewToViewList(View view) {  
    	Cookie cookie = getUserCookie();    
    	if(cookie == null){
    		cookie = setUserCookie();
    	}
    	view.setCookie(cookie);
    	viewRepo.save(view);
    	log.debug("addViewToViewList");    	
    }
    	
    /**
     * Add or Edit a custom law to/of the current view
     * @param Law
     */
    public void addLawToView(Law law) {		
		View currentView = viewRepo.getOne(getUserCookie().getCurrentViewId());		
		lawRepo.save(law);
		currentView.addLaw(law);		
        viewRepo.save(currentView);
    }    
    
    /**
     * Adds a list of laws to the current view 
     * Attention: This method is used to save the laws of the SearchResponse
     * @param lawList
     */	
    public void saveLawListToView(List<Law> lawList) {		
		View currentView = viewRepo.getOne(getUserCookie().getCurrentViewId());			
		lawRepo.save(lawList);
		for(Law law : lawList){
			currentView.addLaw(law);
		}				
        viewRepo.save(currentView);
    } 
    
    /**
     * Get's the currently running search
     * @return Search
     */
    public Search getCurrentSearch() {
    	Long id = getUserCookie().getCurrentSearchId();
    	if (id == null)
    		return null;
    	return searchRepo.getOne(id);
    }
   
    /**
     * Set the currently running search
     * Attention: A new search should only be start if the current one
     * is finalized or null!    
     * @param new current search Search
     */
    public void setCurrentSearch(Search currentSearch) { 
    	Cookie cookie = getUserCookie();
    	cookie.setCurrentSearchId(currentSearch.getId());
    	cookieRepo.save(cookie);
    }
}
