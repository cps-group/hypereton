package io.github.cpsgroup.hypereton.service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import io.github.cpsgroup.hypereton.connector.SearchResponse;
import io.github.cpsgroup.hypereton.domain.Law;
import io.github.cpsgroup.hypereton.domain.Search;
import io.github.cpsgroup.hypereton.repository.LawRepository;
import io.github.cpsgroup.hypereton.repository.SearchRepository;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class RemoteService {
    private final Logger log = LoggerFactory.getLogger(RemoteService.class);       
    @Inject
    private SearchRepository searchRepo;    
    @Inject
    private LawRepository lawRepo;     
    @Inject
    private CookieService cookieService;  
//    private Connector connector= new Connector();
    private boolean stopSign;
	private static final ExecutorService workers = Executors.newCachedThreadPool();
    
    
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>    
    /**
     * Start a remote query with for a given search term on an external data provider (e.g. Makrolog)
     * @param Search
     */
    public void makeRemoteQuery(Search search){
    //if search.getProvider().equals(Makrolog) >> connector = new MakrologConnector();
    	log.debug("REMOTE: Remote Search initialized: {}", search);
    	
    //Start Connector (Makrolog) search with given searchTerm    	
    	SearchResponse searchRsp = simulateSearchResponse(); // = connector.startSearch(search.getSearchTerm());  
    	log.debug("REMOTE: Search Finished. Saving SearchResponse in Local Database");
    	
    //Check whether a law already exists in local database
    	//1.Check whether the laws already exist in DB 	 	
    	searchRsp = checkResonse(searchRsp);
    	
	//Save search response in local DB
    	cookieService.saveLawListToView(searchRsp.getLawList());    	
    	search.setNumFound(searchRsp.getNumReturned()); //TODO
    	search.setLawList(searchRsp.getLawList());
    	searchRepo.save(search);
    }    

    
    /**
     * Checks the search response: 
     * whether the laws already exist
     * @param searchRsp
     * @return filtered searchRsp
     */
    public SearchResponse checkResonse(SearchResponse searchRsp){
    	List<Law> checkedLawList = new ArrayList<Law>();
    	for(Law law : searchRsp.getLawList()){
    		Law x = lawRepo.containsByExternId(law.getExternId());
    		if(x==null){
    			//Law is new
    			checkedLawList.add(law);    			
    		}else{
    			//Law already exists
    			checkedLawList.add(x);
    		}    			    			
    	}
    	searchRsp.setLawList(checkedLawList);
    	return searchRsp;
    }
      
    
    /**
     * SIMULATION
     * @return
     */
    public SearchResponse simulateSearchResponse(){
    	int i=5;
    	SearchResponse searchRsp = new SearchResponse();
    	searchRsp.setNumFound(i);
    	searchRsp.setNumReturned(i);
    	
    	List<Law> lawList = new ArrayList<Law>();
    	
    	while(i>0){
    		lawList.add(new Law("SolrIdXX"+i, "LawXX"+i, "Hessen", "http://www.google.de", "24:04:2014"));
    		i--;
    	}
    	searchRsp.setLawList(lawList);
    	return searchRsp;
    }
    
    
    public void startProcessing(){
    	stopSign=false;    	
    	List<Law> lawList = searchRepo.getNonProcessedLaws(searchRepo.getRunning()).getLawList();	    	
    	System.out.println(">>>>>>>>>>>>>>startProcessing>>>>>>>NUMBER OF LAWS TO PROCESS>>>>>>>>>>>>>>>>>>>>"+lawList.size());
    	processingAll(lawList);
    }         

    
    public void pauseProcessing(){
    	stopSign=true;
    }
    
    
    public void finishProcessing(){    	
    	Search finishedSearch = searchRepo.getRunning();
    	finishedSearch.setFinalized(true);
    	finishedSearch.setRunning(false);
    	searchRepo.save(finishedSearch);
    	
    	//Close other stuff (downloader)
    }
    
    
    public void saveProcessedLaws(List<Law> lawBlock){
    	for(Law law : lawBlock){
    		lawRepo.save(law);   		
    	}    	
    }
    
    
    public List<Law> simConnectorDownload(List<Law> lawBlock){
    	return lawBlock;
    }
    
    
    public void processingAll(List<Law> lawList){
    	List<Law> lawBlock = new ArrayList<Law>();
    	int numLeft=lawList.size();
    	int blocksize=3;
    	int count=0;
    	
    	for(Law law : lawList){
    		lawBlock.add(law);
    		count++;
    		if(count%blocksize==0 || numLeft==1){    			
    			try {
    				processLawBlock(lawBlock);	
    				saveProcessedLaws(lawBlock);
    			}catch (InterruptedException e) {
					e.printStackTrace();
				}catch (ExecutionException e) {
					e.printStackTrace();
				}
    			lawBlock.clear();    			
    		}
        	if(stopSign==true){        		
        		break;
        	}
        	numLeft--;
    	}
    	finishProcessing();
    }


	public List<Law> processLawBlock(final List<Law> lawBlock) throws InterruptedException, ExecutionException{
		
		Callable<List<Law>> downloadLawBlock = new Callable<List<Law>>(){
			public List<Law> call()throws Exception {
				System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>processLawBlock>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+lawBlock);
				Thread.sleep(2000);
				return simConnectorDownload(lawBlock);
				}
			};
			    
			    Future<List<Law>> listOfFutureLawsDownloaded = workers.submit(downloadLawBlock);
			    return listOfFutureLawsDownloaded.get();
    }

}
    


