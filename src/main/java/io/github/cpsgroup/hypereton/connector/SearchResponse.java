package io.github.cpsgroup.hypereton.connector;

import io.github.cpsgroup.hypereton.domain.Law;
import java.util.List;

/** 
 *A POJO class for the different kind of data of the search response
 */

public class SearchResponse {
	private int numFound;
	private int numReturned;
	private List<String> facetList; 
	private List<Law> lawList;	
	
	public int getNumFound() {
		return numFound;
	}
	public void setNumFound(int numFound) {
		this.numFound = numFound;
	}
	public int getNumReturned() {
		return numReturned;
	}
	public void setNumReturned(int numReturned) {
		this.numReturned = numReturned;
	}
	public List<String> getFacetList() {
		return facetList;
	}
	public void setFacetList(List<String> facetList) {
		this.facetList = facetList;
	}
	public List<Law> getLawList() {
		return lawList;
	}
	public void setLawList(List<Law> lawList) {
		this.lawList = lawList;
	}
}
