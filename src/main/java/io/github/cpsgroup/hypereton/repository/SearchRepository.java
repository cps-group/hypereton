package io.github.cpsgroup.hypereton.repository;

import java.util.List;

import io.github.cpsgroup.hypereton.domain.Cookie;
import io.github.cpsgroup.hypereton.domain.Search;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 * Spring Data JPA repository for the Search entity.
 */
public interface SearchRepository extends JpaRepository<Search, Long> {
	
	@Transactional(readOnly=true)
	public List<Search> findAllByCookie(Cookie cookie);
	
	@Transactional(readOnly=true)
	@Query("SELECT s FROM Search s WHERE s.cookie = (:cookie) ORDER BY s.id DESC")
	public List<Search> findAllByCookieOrderById(@Param("cookie") Cookie cookie);
	
	@Transactional(readOnly=true)
	@Query("SELECT s FROM Search s WHERE s.running=true")
	public Search getRunning();
	
	@Transactional(readOnly=true)
	@Query("SELECT COUNT(s) FROM Search s WHERE s.running=true")
	public int getNumRunning();
	
	@Transactional(readOnly=true)
	@Query("SELECT s FROM Search s JOIN FETCH s.lawList l WHERE s.id = (:id)")
	public Search getSearchEAGER(@Param("id") Long searchId);
	
	@Transactional(readOnly=true)
	@Query("SELECT s FROM Search s JOIN FETCH s.lawList l WHERE s=(:search) AND l.processedFlag=false")
	public Search getNonProcessedLaws(@Param("search") Search search);
		
	@Transactional(readOnly=true)
	@Query("SELECT s FROM Search s JOIN FETCH s.lawList l WHERE s=(:search) AND l.processedFlag=true")
	public Search getProcessedLaws(@Param("search") Search search);
}
