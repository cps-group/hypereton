package io.github.cpsgroup.hypereton.repository;

import java.util.List;

import io.github.cpsgroup.hypereton.domain.Cookie;
import io.github.cpsgroup.hypereton.domain.View;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Spring Data JPA repository for the View entity.
 */
public interface ViewRepository extends JpaRepository<View, Long> {
	
	@Query
	public List<View> findAllByCookie(Cookie cookie);
	
	@Query("SELECT v FROM View v JOIN FETCH v.lawList l WHERE v = (:view)")
	public View getViewEAGER(@Param("view") View view);

	@Query("SELECT v FROM View v JOIN FETCH v.lawList l WHERE v.id = (:viewid)")
	public View getViewEAGER(@Param("viewid") Long viewid);
	}
