package io.github.cpsgroup.hypereton.repository;

import io.github.cpsgroup.hypereton.domain.Cookie;
import io.github.cpsgroup.hypereton.domain.User;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * Spring Data JPA repository for the Cookie entity.
 */
public interface CookieRepository extends JpaRepository<Cookie, Long> {
	
	@Query
	public Cookie findCookieByUser(User user);

}
