package io.github.cpsgroup.hypereton.repository;

import java.util.List;

import io.github.cpsgroup.hypereton.domain.Law;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 * Spring Data JPA repository for the Law entity.
 */
public interface LawRepository extends JpaRepository<Law, Long> {	
	
	@Transactional(readOnly=true)
	public List<Law> findByTitle(String title);
	
	@Transactional(readOnly=true)
	@Query("SELECT l FROM Law l WHERE l.externId = (:ext)")
	public Law containsByExternId(@Param("ext") String externId);	
}
