package io.github.cpsgroup.hypereton.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.joda.deser.LocalDateDeserializer;

import io.github.cpsgroup.hypereton.domain.util.CustomLocalDateSerializer;

import org.hibernate.annotations.Type;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;

import javax.persistence.*;
import javax.validation.constraints.Size;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * A Law.
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "T_LAW")
public class Law implements Serializable {	
//    /**
//     * Inverse Side 
//     */
//    @JsonIgnore
//    @ManyToMany(mappedBy= "lawList")
//    private List<View> viewList = new ArrayList<View>();
        
//    /**
//     * Inverse Side 
//     */
//    @JsonIgnore
//    @ManyToMany(mappedBy= "lawList")
//    private List<Search> searchList = new ArrayList<Search>();
    
	@Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.TABLE)
    private long id;
	
    /**
     * The string representation of the Id/PrimaryKey/URI the document 
     * has in the datasource of the external data provider (e.g. Makrolog)
     */
    @Size(min = 1, max = 50)
    @Column(name = "extern_id") //unique=true
    private String externId;

    @Size(min = 1, max = 50)
    @Column(name = "title")
    private String title;    
    
    /**
     * Bund or Bundesland(e.g "Hessen", "Saarland", "Bund"
     */
    @Size(min = 1, max = 50)
    @Column(name = "region")
    private String region;
    
    @Size(min = 1, max = 50)
    @Column(name = "url")
    private String url; 
    
    @Column(name = "num_sections")
    private int numSections;
    
    /**
     * The creation date of the law text - The date it was signed by the government!
     */
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = CustomLocalDateSerializer.class)
    @Column(name = "ratification_date")
    private LocalDate ratificationDate; 
    
    /**
     * Is the law text a custom creation by the user or was it fetched from an offical data provider
     */
    @Column(name = "custom_flag")
    private boolean customFlag;

    /*
     * Is the law processed or not
     */
    @Column(name = "processed_flag")
    private boolean processedFlag;
    
    
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>   
    /**
     *  Constructor to create laws with data from official data providers
     * @param externId
     * @param title
     * @param region
     * @param url
     * @param numbSections
     * @param date
     */
    public Law(String externId, String title, String region, String url, String date){
		   this.externId = externId;
		   this.title = title;
		   this.region = region;
		   this.url = url;
		   this.ratificationDate = new LocalDate();
		   
		   this.numSections = 0;
		   this.processedFlag = false;		   		   
		   this.customFlag = false;
   }
    
    private Law(){    	
    }
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

//    public List<Search> getSearchList(){
//    	return searchList;
//    }
//    
//    public void setSearchList(List<Search> searchList){
//    	this.searchList = searchList;
//    }    
    
//    public List<View> getViewList(){
//    	return viewList;
//    }    
//    
//    public void setViewList(List<View> viewList){
//    	this.viewList = viewList;
//    } 
    
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDate getRatificationDate() {
        return ratificationDate;
    }

    public void setRatificationDate(LocalDate ratificationDate) {
        this.ratificationDate = ratificationDate;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }
    
    public int getNumSections() {
        return numSections;
    }

    public void setNumSection(int numSections) {
        this.numSections = numSections;
    }
    
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    
    public boolean getCustomFlag() {
        return customFlag;
    }

    public void setCustomFlag(boolean customFlag) {
        this.customFlag = customFlag;
    }
    
    public String getExternId() {
        return externId;
    }

    public void setExternId(String externId) {
        this.externId = externId;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Law law = (Law) o;

        if (id != law.id) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return "Law{" +
                "id=" + id +
                ", Title='" + title +
                '}';
    }
}
