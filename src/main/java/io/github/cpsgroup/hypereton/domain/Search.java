package io.github.cpsgroup.hypereton.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.joda.deser.LocalDateDeserializer;

import io.github.cpsgroup.hypereton.domain.util.CustomLocalDateSerializer;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * A Query.
 * This entity contains all Searches of all Users. 
 * These Searches were used to retrieve Laws from Makrolog or other data providers
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "T_SEARCH")
public class Search implements Serializable {
	@PreRemove
    private void preRemove() {
		//Delete currentView of cookie		
		Cookie delCookie = getCookie();
		if(delCookie.getCurrentSearchId() != null && delCookie.getCurrentSearchId() == this.id)
			delCookie.setCurrentSearchId(null);			
	}
	
	/**
     * Owning Side
     * This entity is in control of the relationship query_has_law
     */
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name="search_has_law",
            joinColumns=
            @JoinColumn(name="search_id", referencedColumnName="id"),
      inverseJoinColumns=
            @JoinColumn(name="law_id", referencedColumnName="id")
    )
    private List<Law> lawList = new ArrayList<Law>();
	
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="cookie_id")
    private Cookie cookie;
    
    @Id
    @Column(name="id", nullable=false)
    @GeneratedValue(strategy = GenerationType.TABLE)
    private long id; 
    
    /**
     * search string of the query
     */
    @Size(min=1, max=50)
    @Column(name="search_term")
    private String searchTerm;
    
    /**
     * Name of the Data Provider (e.g. Makrolog)
     */
    @Size(min=1, max=50)
    @Column(name="data_provider")
    private String dataProvider;
        
    /**
     * Number of results found
     */
    @Column(name="num_found")
    private int numFound;
    
    /**
     * Number of results processed
     */
    @Column(name="num_processed")
    private int numProcessed;
    
    /**
     * All results downloaded and sections counted: true/false
     */
    @Column(name="finalized")
    private boolean finalized;
    
    /**
     * All results downloaded and sections counted: true/false
     */
    @Column(name="running")
    private boolean running;
    
     /**
      * Creation date when the search was made
      */
    @NotNull
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = CustomLocalDateSerializer.class)
    @Column(name = "creation_date")
    private LocalDate creationDate;   
    
    
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<Law> getLawList(){
    	return lawList;
    }
    
    public void setLawList(List<Law> lawList){
    	this.lawList = lawList;
    }
    
    public void addLaw(Law law){
    	if(!this.lawList.contains(law)){
    		this.lawList.add(law);
    	}
    }  
    
    public void removeLaw(Law law){  
    	if(this.lawList.contains(law)){
    		this.lawList.remove(law);
    	}
    }    

    public Cookie getCookie() {
        return cookie;
    }
    
    public void setCookie(Cookie cookie) {
        this.cookie = cookie;
    }
    
    public String getSearchTerm() {
        return searchTerm;
    }

    public void setSearchTerm(String searchTerm) {
        this.searchTerm = searchTerm;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;        
    }
    
    public String getDataProvider() {
        return dataProvider;
    }

    public void setDataProvider(String dataProvider) {
        this.dataProvider = dataProvider;
    }      

    public int getNumFound() {
        return numFound;
    }

    public void setNumFound(int numFound) {
        this.numFound = numFound;
    }
    
    public int getNumProcessed() {
        return numProcessed;
    }

    public void setNumProcessed(int numProcessed) {
        this.numProcessed = numProcessed;
    }
    
    public boolean getFinalized() {
        return finalized;
    }

    public void setFinalized(boolean finalized) {
        this.finalized = finalized;
    }
    
    public boolean getRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }
    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Search search = (Search) o;

        if (id != search.id) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return searchTerm.hashCode();
    }

    @Override
	public String toString() {
		return "Search [id="+ id + 
				", searchTerm=" + searchTerm + 
				", dataProvider=" + dataProvider + 
				", numFound=" + numFound + 
				", numProcessed=" + numProcessed + "]";
	}
}
