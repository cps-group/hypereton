package io.github.cpsgroup.hypereton.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.joda.deser.LocalDateDeserializer;

import io.github.cpsgroup.hypereton.domain.util.CustomLocalDateSerializer;
import io.github.cpsgroup.hypereton.repository.ViewRepository;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;

import javax.inject.Inject;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;



/**
 * A View.
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "T_VIEW")
public class View implements Serializable {
	@PreRemove
    private void preRemove() {
		//Delete currentView of cookie		
		Cookie delCookie = getCookie();
		if(delCookie.getCurrentViewId() != null && delCookie.getCurrentViewId() == this.id)
			delCookie.setCurrentViewId(null);			
	}
	
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="cookie_id")
    private Cookie cookie;
    
    /**
     * Owning Side
     * This entity is in control of the relationship view_has_documents
     */
    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY) //, 
    @JoinTable(name="view_has_law",
    joinColumns= @JoinColumn(name="view_id", referencedColumnName="id"),
    inverseJoinColumns= @JoinColumn(name="law_id", referencedColumnName="id"))
    private List<Law> lawList = new ArrayList<Law>();

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private long id;   
    
    @Size(min = 1, max = 50)
    @Column(name = "title")
    private String title;    
    
    @NotNull
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = CustomLocalDateSerializer.class)
    @Column(name = "edit_date")
    private LocalDate editDate;  
    
    @Size(min = 1, max = 100)
    @Column(name = "description")
    private String description;
    
    
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    
    public Cookie getCookie() {
        return cookie;
    }
    
    public void setCookie(Cookie cookie) {
        this.cookie = cookie;
    }
    
    public List<Law> getLawList(){
    	return lawList;
    }
    
    public void setLawList(List<Law> lawList){
    	this.lawList = lawList;
    }
    
    public void addLaw(Law law){
    	if(!this.lawList.contains(law)){
    		this.lawList.add(law);
    	}
    }    
    
    public void removeLaw(Law law){  
    	if(this.lawList.contains(law)){
    		this.lawList.remove(law);
    	}
    }
    
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getEditDate() {
        return editDate;
    }

    public void setEditDate(LocalDate editDate) {
        this.editDate = editDate;
    }
    
    public View merge(View view){
    	this.setTitle(view.getTitle());
    	this.setDescription(view.getDescription());
    	this.setEditDate(view.getEditDate());    	
    	return this;    	
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        View view = (View) o;

        if (id != view.id) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return "View{" +
                "id=" + id +
                ", Title='" + title + '\'' +               
                ", Edit Date=" + editDate +
                '}';
    }
}
