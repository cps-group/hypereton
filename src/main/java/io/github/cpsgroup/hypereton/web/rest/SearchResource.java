package io.github.cpsgroup.hypereton.web.rest;

import com.codahale.metrics.annotation.Timed;
import io.github.cpsgroup.hypereton.domain.Search;
import io.github.cpsgroup.hypereton.repository.SearchRepository;
import io.github.cpsgroup.hypereton.service.CookieService;
import io.github.cpsgroup.hypereton.service.RemoteService;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * REST controller for managing Search.
 */
@RestController
@RequestMapping("/app")
public class SearchResource {
    private final Logger log = LoggerFactory.getLogger(SearchResource.class);
    @Inject
    private SearchRepository searchRepo;    
    @Inject
    private RemoteService remoteService;
    @Inject
    private CookieService cookieService;
    
    
    //>>>>>>>>>>>>>>>>>>>>>>>>>NEEDED FUNCTIONALITY>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    /**
     * POST  /rest/searchs -> MAKE a new search.
     */
    @RequestMapping(value = "/rest/searchs",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void create(@RequestBody Search newSearch) {
    	log.debug("REST request to start Search: {}", newSearch);
        
        if(searchRepo.getNumRunning()==0) {
        	//Allowed to start NEW search        	
        	newSearch.setFinalized(false);            
        	newSearch.setCreationDate(new LocalDate());
        	newSearch.setCookie(cookieService.getUserCookie()); 
            
            try {
				Thread.sleep(0);
				remoteService.makeRemoteQuery(newSearch);
			} catch (InterruptedException e) {
				newSearch.setRunning(false);
				e.printStackTrace();
			}            
            searchRepo.save(newSearch);                          	
        }      
    }
    
    /**
     * GET  /rest/searchs -> Shows the search history of the user.
     */
    @RequestMapping(value = "/rest/searchs",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Search> getAll() {
        log.debug("REST request to get all Searchs");
        return searchRepo.findAllByCookieOrderById(cookieService.getUserCookie());
    }
    
    /**
     * DELETE  /rest/searchs/:id -> delete the "id" search with All it's laws.
     */
    @RequestMapping(value = "/rest/searchs/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete Search : {}", id);
        searchRepo.delete(id);
    }
    
    /**
     * GET  /rest/searchs/:id -> ATENTION>>> This REST Function is not for receiving a law with a given id 
     * This function is a test area to control background processing from within the UI
     */
    @RequestMapping(value = "/rest/searchs/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Search> get(@PathVariable Long id, HttpServletResponse response) {
        log.debug("REST request to get Search : {}", id);
        Search search = searchRepo.findOne(id);
        if (search == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        
        //TODO
        //This
        //This part should start and pause background thread for processing for downloading and section counting
        //ATENTION Unstable! 
        if(search.getRunning()==true){
        	//This have to be the current search and it's running
        	//So PAUSE processing
        	search.setRunning(false);
        	searchRepo.saveAndFlush(search);
        	
        	remoteService.pauseProcessing();        	
        	return new ResponseEntity<>(HttpStatus.OK);
        	
        	}
        else if(searchRepo.getNumRunning() == 0){
        	//No processing running
        	//So START processing   
        	search.setRunning(true);
        	searchRepo.saveAndFlush(search);
        	
        	remoteService.startProcessing();
        	return new ResponseEntity<>(HttpStatus.OK);
        	}
        	             	
        //This processing cant't be started because it's another is already running
        return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>GENERATED FUNCTIONALITY>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    
//    /**
//     * GET  /rest/searchs/:id -> get the "id" search.
//     */
//    @RequestMapping(value = "/rest/searchs/{id}",
//            method = RequestMethod.GET,
//            produces = MediaType.APPLICATION_JSON_VALUE)
//    @Timed
//    public ResponseEntity<Search> get(@PathVariable Long id, HttpServletResponse response) {
//        log.debug("REST request to get Search : {}", id);
//        Search search = searchRepo.findOne(id);
//        if (search == null) {
//            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//        }
//        return new ResponseEntity<>(search, HttpStatus.OK);
//    }
}
