/**
 * Data Access Objects used by Spring MVC REST controllers.
 */
package io.github.cpsgroup.hypereton.web.rest.dto;
