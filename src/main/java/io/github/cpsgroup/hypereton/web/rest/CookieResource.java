package io.github.cpsgroup.hypereton.web.rest;

import com.codahale.metrics.annotation.Timed;
import io.github.cpsgroup.hypereton.domain.Cookie;
import io.github.cpsgroup.hypereton.repository.CookieRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * REST controller for managing Cookie.
 */
@RestController
@RequestMapping("/app")
public class CookieResource {
    private final Logger log = LoggerFactory.getLogger(CookieResource.class);
    @Inject
    private CookieRepository cookieRepository;

    
    //>>>>>>>>>>>>>>>>>>>>>>>>>NEEDED FUNCTIONALITY>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>     
   
    
    //>>>>>>>>>>>>>>>>>>>>>>>>>GENERATED FUNCTIONALITY>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    /**
     * POST  /rest/cookies -> Create a new cookie.
     */
    @RequestMapping(value = "/rest/cookies",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void create(@RequestBody Cookie cookie) {
        log.debug("REST request to save Cookie : {}", cookie);
        cookieRepository.save(cookie);
    }

    /**
     * GET  /rest/cookies -> get all the cookies.
     */
    @RequestMapping(value = "/rest/cookies",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Cookie> getAll() {
        log.debug("REST request to get all Cookies");
        return cookieRepository.findAll();
    }

    /**
     * GET  /rest/cookies/:id -> get the "id" cookie.
     */
    @RequestMapping(value = "/rest/cookies/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Cookie> get(@PathVariable Long id, HttpServletResponse response) {
        log.debug("REST request to get Cookie : {}", id);
        Cookie cookie = cookieRepository.findOne(id);
        if (cookie == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(cookie, HttpStatus.OK);
    }

    /**
     * DELETE  /rest/cookies/:id -> delete the "id" cookie.
     */
    @RequestMapping(value = "/rest/cookies/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete Cookie : {}", id);
        cookieRepository.delete(id);
    }
}
