package io.github.cpsgroup.hypereton.web.rest;

import com.codahale.metrics.annotation.Timed;

import io.github.cpsgroup.hypereton.domain.Law;
import io.github.cpsgroup.hypereton.domain.Search;
import io.github.cpsgroup.hypereton.domain.View;
import io.github.cpsgroup.hypereton.repository.LawRepository;
import io.github.cpsgroup.hypereton.repository.SearchRepository;
import io.github.cpsgroup.hypereton.repository.ViewRepository;
import io.github.cpsgroup.hypereton.service.CookieService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import java.util.List;

/**
 * REST controller for managing Law.
 */
@RestController
@RequestMapping("/app")
public class LawResource {
    private final Logger log = LoggerFactory.getLogger(LawResource.class);
    @Inject
    private LawRepository lawRepo;    
    @Inject
    private ViewRepository viewRepo; 
    @Inject
    private SearchRepository searchRepo;
    @Inject
    private CookieService cookieService;
    
    
    
    //>>>>>>>>>>>>>>>>>>>>>>>>>NEEDED FUNCTIONALITY>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>    
    /**
     * POST  /rest/laws -> CREATE or EDIT a custom law.
     */
    @RequestMapping(value = "/rest/laws",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void create(@RequestBody Law law) {
        log.debug("REST request to save Law : {}", law);
        if(law.getCustomFlag()){
        	//Only allowed to create/edit custom laws
        	cookieService.addLawToView(law);
        }
    }

    /**
     * GET  /rest/laws -> GET ALL the laws of the currentView.
     */
    @RequestMapping(value = "/rest/laws",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Law> getAll() {
        log.debug("REST request to get all Laws of the current View");  
        View view = viewRepo.getViewEAGER(cookieService.getCurrentView().getId());
        if(view==null || view.getLawList().isEmpty())
        	return null;
        return view.getLawList();
    }
    
    /**
     * DELETE  /rest/laws/:id -> REMOVE the "id" law from the currentView.
     */
    @RequestMapping(value = "/rest/laws/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete Law : {}", id);
        View view = viewRepo.getViewEAGER(cookieService.getCurrentView().getId());        
        Law law = lawRepo.findOne(id);
        view.removeLaw(law);
        viewRepo.save(view);
    }
        
    /**
     * GET  /rest/laws -> GET ALL the laws of the currentSearch
     */
    @RequestMapping(value = "/rest/laws/ofSearch",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Law> getAllOfSearch() {
        log.debug("REST request to get all Laws of the current search");  
        Search search = searchRepo.getSearchEAGER(cookieService.getCurrentSearch().getId());        
        if(search==null || search.getLawList().isEmpty())
        	return null;
        return search.getLawList();
    }    
    
    //>>>>>>>>>>>>>>>>>>>>>>>>>GENERATED FUNCTIONALITY>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    /**
     * GET  /rest/laws/:id -> get the "id" law.
     */
    @RequestMapping(value = "/rest/laws/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Law> get(@PathVariable Long id, HttpServletResponse response) {
        log.debug("REST request to get Law : {}", id);
        Law law = lawRepo.findOne(id);
        if (law == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(law, HttpStatus.OK);
    }
}
