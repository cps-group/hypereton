package io.github.cpsgroup.hypereton.web.rest;

import com.codahale.metrics.annotation.Timed;

import io.github.cpsgroup.hypereton.domain.Law;
import io.github.cpsgroup.hypereton.domain.View;
import io.github.cpsgroup.hypereton.repository.LawRepository;
import io.github.cpsgroup.hypereton.repository.ViewRepository;
import io.github.cpsgroup.hypereton.service.CookieService;

import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * REST controller for managing View.
 */
@RestController
@RequestMapping("/app")
public class ViewResource {
    private final Logger log = LoggerFactory.getLogger(ViewResource.class);
    @Inject
    private ViewRepository viewRepo;    
    @Inject
    private CookieService cookieService;
    @Inject
    private LawRepository lawRepo;
    
    
    
    //>>>>>>>>>>>>>>>>>>>>>>>>>NEEDED FUNCTIONALITY>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>    
    /**
     * POST  /rest/views -> Create/Update a view for logged in User.
     */
    @RequestMapping(value = "/rest/views",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void createView(@RequestBody View newView) { 
    	log.debug("REST request to save/edit View: ", newView);
    	newView.setEditDate(new LocalDate());
    	
    	View existingView = viewRepo.getViewEAGER(newView.getId());
    	if(existingView == null){
    		//Newly created view OR view with no laws 		
        	cookieService.addViewToViewList(newView);          	
    	}else{
    		//Edited view with laws in it
    		existingView.merge(newView);
    		viewRepo.save(existingView);		   		
    	}    	   
    	cookieService.setCurrentView(newView);         
    }    
    
    /**
     * GET  /rest/views -> get all views of logged in User
     */
    @RequestMapping(value = "/rest/views",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<View> getAll() {
    	log.debug("REST request to get all Views of User");    
    	return viewRepo.findAllByCookie(cookieService.getUserCookie()); 
    }
    
    /**
     * DELETE  /rest/views/:id -> delete the "id" view.
     */
    @RequestMapping(value = "/rest/views/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete View : {}", id);
        viewRepo.delete(id);
    }
    
    /**
     * GET  /rest/views/:id -> get the "id" view to shows its data online
     */
    @RequestMapping(value = "/rest/views/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<View> get(@PathVariable Long id, HttpServletResponse response) {
        log.debug("REST request to get View : {}", id);
        View view = viewRepo.findOne(id);
        if (view == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(view, HttpStatus.OK);
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>GENERATED FUNCTIONALITY>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>    

}
