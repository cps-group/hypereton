#!/bin/bash
#java -Dserver.port=$PORT -Dspring.datasource.url=$DATABASE_URL -Dspring.profiles.active=$PROFILE $JAVA_OPTS -jar target/hypereton-0.1-SNAPSHOT.war
#java -Dserver.port=$PORT -Dspring.datasource.databaseName=$dbName -Dspring.datasource.serverName=$serverName -Dspring.datasource.username=$userName -Dspring.datasource.password=$password -Dspring.profiles.active=prod $JAVA_OPTS -jar target/hypereton-0.1-SNAPSHOT.war
java $JAVA_OPTS -Dserver.port=$PORT -Dspring.profiles.active=prod -Dspring.datasource.username=$username -Dspring.datasource.password=$password -jar target/hypereton-0.1-SNAPSHOT.war
